CREATE TABLE `Projects` (
  `id`       INT(11)                                NOT NULL AUTO_INCREMENT,
  `title`    VARCHAR(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abstract` VARCHAR(1024) COLLATE utf8mb4_unicode_ci        DEFAULT NULL,
  `lang`     VARCHAR(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img`      VARCHAR(64) COLLATE utf8mb4_unicode_ci          DEFAULT NULL,
  `url`      VARCHAR(100) COLLATE utf8mb4_unicode_ci         DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `title` (`title`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

INSERT INTO `Projects` VALUES
  (1, 'httplus', 'Une librairie HTTP simple, écrite en C++, pour C++.', 'C++', NULL,
   'https://github.com/mdubourg001/httplus'),
  (2, 'discord_runeforge',
   'Un bot discord parcourant runeforge.gg à la recherche des builds de runes des champions League Of Legends.',
   'Javascript', './static/images/discord_runeforge.png', 'https://github.com/mdubourg001/discord_runeforge_gg'),
  (3, 'simple_clock', 'Une librairie C++ rendant simple la manipulation des évenements temporels.', 'C++', '',
   'https://github.com/mdubourg001/simple_clock'),
  (4, 'runner', 'Le jeu du Runner.', 'C++ / SFML', './static/images/runner.png',
   'https://github.com/mdubourg001/Runner'),
  (5, 'leboncoin_spider',
   "Un script Python parcourant Leboncoin.fr. L\'url lui étant fourni sera parcouru toute les 30 secondes en vous envoyant des mails en cas de nouveaux résultats.",
   'Python 3', NULL, 'https://github.com/mdubourg001/leboncoin_spider'),
  (6, 'tarot_deal', "L\'animation de distribution du jeu de Tarot en Java.", 'Java / JavaFX', NULL,
   'https://github.com/mdubourg001/TarotDeal'),
  (7, 'camping_simulator', "Logiciel de gestion d\'un camping. Adaptable à tout camping via l\'import de la carte.",
   'Java / JavaFX', NULL, 'https://github.com/Moccko/campingsimulator2017'),
  (8, 'e_commerce_web', "Site (factice) de vente d\'albums. Développé dans le cadre d\'un projet universitaire.",
   'PHP (Symfony)', NULL, 'https://github.com/Sydpy/ECommerceWeb'),
  (9, 'shortcut_maker',
   'Programme simple écrit en C++, permettant de créer des raccourcis pour les programmes sous la plupart des distributions Linux.',
   'C++ / Qt4', NULL, 'https://github.com/mdubourg001/ShortcutMaker');

COMMIT;