window.onload = function () {

    /* portfolio-div background utilities */
    document.getElementById("portfolio-twisted-background").style.height
        = document.getElementById("portfolio-div").offsetHeight + "px";


    window.onresize = function () {
        document.getElementById("portfolio-twisted-background").style.height
            = document.getElementById("portfolio-div").offsetHeight + "px";
    };


    /* =========== animations =========*/

    document.getElementById('navbar').classList.add('displayed');

    setTimeout(function () {
        document.getElementById('presentation-div').classList.add('displayed');
    }, 0);

    setTimeout(function () {
        document.getElementById('catchphrase').classList.add('displayed');
    }, 800);

    setTimeout(function () {
        var st = document.getElementsByClassName('section-title');
        [].forEach.call(st, function (s) {
            s.classList.add('displayed');
        });
    }, 600);

    setTimeout(function () {
        document.getElementById('first-skill-column').classList.add('displayed');
    }, 1000);

    setTimeout(function () {
        document.getElementById('second-skill-column').classList.add('displayed');
    }, 1500);

    setTimeout(function () {
        document.getElementById('third-skill-column').classList.add('displayed');
    }, 2000);

    setTimeout(function () {
        var p = document.getElementsByClassName('project');
        [].forEach.call(p, function (s) {
            s.classList.add('displayed');
        });
    }, 600);
};

