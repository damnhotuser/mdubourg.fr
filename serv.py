from flask import Flask, render_template
import sqlite3

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def index():
    conn = sqlite3.connect('portfolio.db')
    cursor = conn.cursor()
    projects = []

    for row in cursor.execute('SELECT * FROM Projects'):
        print(row)
        p = {
            "id": row[0],
            "title": row[1],
            "abstract": row[2],
            "lang": row[3],
            "img": row[4],
            "url": row[5]
        }

        projects.append(p)

    cursor.close()
    conn.close()

    return render_template('index.html', projects=projects, name='index')


if __name__ == "__main__":
    app.run(host='127.0.0.1')
